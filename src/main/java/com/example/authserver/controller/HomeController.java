package com.example.authserver.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @GetMapping("/home")
    public ModelMap homepage(Authentication currentUser){
        LOGGER.info("Current User : {}", currentUser);
        return new ModelMap()
                .addAttribute("currentUser", ((User)currentUser.getPrincipal()).getUsername());
    }
}

