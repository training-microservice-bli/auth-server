package com.example.authserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.security.KeyPair;

@SpringBootApplication
public class AuthserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthserverApplication.class, args);
	}

	@Value("${keystore.file}") private Resource keystoreFile;
	@Value("${keystore.password}") private String keystorePassword;
	@Value("${keystore.alias}") private String keypairAlias;

	@Bean
	public KeyPair loadKeypair(){
		KeyPair keyPair = new KeyStoreKeyFactory(
				keystoreFile, keystorePassword.toCharArray())
				.getKeyPair(keypairAlias);
		return keyPair;
	}
}
