# Authorization Server #

Flow yang disupport:

* Authorization Code Flow

## Authorization Code ##

1. Akses ke URL Authorization. [http://localhost:8080/oauth/authorize?client_id=clientwebbased&grant_type=authorization_code&response_type=code](http://localhost:8080/oauth/authorize?client_id=clientwebbased&grant_type=authorization_code&response_type=code)

2. Login dengan username `user001` dan password `teststaff`

3. Approve/Deny pilihan scope di consent page

    [![Consent Page](docs/img/consent-page.png)](docs/img/consent-page.png)

4. Browser akan redirect sesuai alamat yang didaftarkan di `redirect_uri`, dengan membawa `code`

5. Tukarkan `code` menjadi `access_token` di URL `http://localhost:8080/oauth/token` dengan parameter :

    * `code`
    * `grant_type` : `authorization_code`
    
    dan header :
    
    * `Authorization` : `Basic Y2xpZW50d2ViYmFzZWQ6YWJjZA==`
    
    Dilakukan dengan Postman seperti ini
    
    [![Postman Get Token](docs/img/get-token.png)](docs/img/get-token.png)

6. Hasilnya seperti ini

    ```json
    {
        "access_token": "2132471f-5edb-4387-93f9-42365273ad39",
        "token_type": "bearer",
        "refresh_token": "8b1d0805-8253-4b27-94bb-c302e80550c8",
        "expires_in": 43199,
        "scope": "entri_data review_transaksi"
    }
    ```

7. Lihat isi token dengan mengakses URL `check_token`. 

    Postman requestnya seperti ini
    
    [![Postman Check Token](docs/img/check-token.png)](docs/img/check-token.png)

    Hasilnya seperti ini

    ```json
    {
        "aud": [
            "belajar"
        ],
        "exp": 1553720996,
        "user_name": "user001",
        "authorities": [
            "VIEW_TRANSAKSI"
        ],
        "client_id": "clientwebbased",
        "scope": [
            "entri_data",
            "approve_transaksi"
        ]
    }
    ```
 
## Implementasi JWT dan JWK ##

1. Membuat keypair dan sekaligus menyimpannya dalam keystore

   ```
    keytool -genkey -alias authserver \
        -keystore src/main/resources/authserver.pfx \
        -storetype PKCS12 \
        -keyalg RSA \
        -keysize 2048 
   ```